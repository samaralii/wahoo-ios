import Foundation


let BASE_URL = "http://hrspidersystem.com/wahoo/"

let PROCESSING = "Processing"
let DELIVERED = "Delivered"
let ON_HOLD = "On Hold"
let CANCELED = "Canceled"
let IS_FIRST_TIME = "is_first_time"

let ID_PROCESSING = 3
let ID_DELIVERED = 4
let ID_ON_HOLD = 5
let ID_CANCELED = 6

var isReturnValue = false
