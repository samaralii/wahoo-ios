import Foundation



func convertArrToJson(_ arr: [_Invoice]) -> String? {
    
    var idList = [String]()
    
    
    for i in arr {
        idList.append(String(describing: i.id!))
    }
    
    let idListJson = IdsList(id: idList)
    let jsonEncoder = JSONEncoder()
    
    
    do {
        let jsonData = try jsonEncoder.encode(idListJson)
        let json = String(data: jsonData, encoding: String.Encoding.utf8)
        
        print(json ?? "")
        
        return json
        
    } catch let error {
        print(error)
    }
    
    
    return nil
}
