import UIKit
import Alamofire


class LoginVC: BaseVC {
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        checkForLogin()
    }
    
    private func checkForLogin() {
        
        print("BOOL = \(getIsFirstTime())")
        
        if getIsFirstTime()  == true {
            openMainView()
        }
        
    }
    
    
    @IBAction func btnLogin(_ sender: Any) {
        
        guard let email = tfEmail.text, !email.isEmpty else {
            return
        }
        
        guard let password = tfPassword.text, !password.isEmpty else {
            return
        }
        
        onLogin(email, password)
        
    }
    
    
    
    
    private func onLogin(_ email: String, _ password: String) {
        
        let parameters = ["email": email,
                          "password": password]
        
        
        let url = "\(BASE_URL)users/applogin"
        
        print("Url \(url) Parameters \(parameters)")
        
        indicator.startAnimating()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON{ response in
            
            switch response.result {
                
            case .success:
                    if let data = response.data {
                    
                    do {

                        let userObj = try JSONDecoder().decode(UserLogin.self, from: data)
                        
                         print(userObj.response)
                        
                        
                        
                        if userObj.response == "success" {
                            self.clearUserData()
                            self.setIsFirstTime(true)
                            self.setUserData(data)
                            self.openMainView()
                        } else {
                            self.showAlert("Error", "Please Try Again")
                        }
                        

                    } catch let error {
                        print(error)
                        self.showAlert("Error", "Please Try Again")
                    }
                    
                    
                }
                
            case .failure(_):
                self.showAlert("Error", "Please Try Again")
                break
            }
            
            self.indicator.stopAnimating()
            
        }
        
    }
    
    private func openMainView() {
      self.performSegue(withIdentifier: "idnMainViewSegue", sender: self)
    }
    
    
}

