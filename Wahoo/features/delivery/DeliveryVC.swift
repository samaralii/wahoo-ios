

import Foundation
import UIKit
import Alamofire
import BarcodeScanner

class DeliveryVC: BaseVC {
    
    var arr = [_Invoice]()
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSearch: UIImageView!
    
    private let refreshControl = UIRefreshControl()
    
    private var isDeliveryView = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        setupTableView()
        
        indicator.startAnimating()
        getDeliveryList()
        setImageViewTap()
        
    }
    
    func setupTableView() {
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        
        
        print("Refresh \(isDeliveryView)")
        refreshList()
       
    }
    
    private func refreshList() {
        if isDeliveryView {
            getDeliveryList()
            print("Delivery Items")
        } else {
            print("Hold Items")
            getHoldItems()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("view Did appear")
        
        if isReturnValue {
            indicator.startAnimating()
            refreshList()
        }
        
    }
    
    
    private func setImageViewTap() {
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(DeliveryVC.tapDetected))
        btnSearch.isUserInteractionEnabled = true
        btnSearch.addGestureRecognizer(singleTap)
    }
    
    @objc func tapDetected() {
        
        let viewController = makeBarcodeScannerViewController()
        viewController.title = "Barcode Scanner"
        present(viewController, animated: true, completion: nil)
        
        
    }
    
    private func makeBarcodeScannerViewController() -> BarcodeScannerViewController {
        let viewController = BarcodeScannerViewController()
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        return viewController
    }
    
    private func getDeliveryList() {
        
        var userId: Int;
        
        if let val = getUserData()?.user.id {
            userId = val
        } else {
            return
        }
        
        let parameters = ["user_id": userId]
        
        
        let url = "\(BASE_URL)invoices/deliveries"
        
        print("Url \(url) Parameters \(parameters)")
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
            
            switch response.result {
                
            case .success:
                if let data = response.data {
                    
                    do {
                        
                        let res = try JSONDecoder().decode(Delivery.self, from: data)
                        
                        print(res.response)
                        
                        if res.invoices.count > 0 {
                            self.arr = res.invoices
                            self.tableView.reloadData()
                        }
                        
                    } catch let error {
                        print(error)
                    }
                    
                    
                }
                
            case .failure(let error):
                print(error)
                
            }
            
            self.indicator.stopAnimating()
            self.refreshControl.endRefreshing()
            
        }
        
    }
    
    private func getHoldItems() {
        
        var userId: Int;
        
        if let val = getUserData()?.user.id {
            userId = val
        } else {
            return
        }
        
        let parameters = ["user_id": userId]
        
        
        let url = "\(BASE_URL)invoices/onhold"
        
        print("Url \(url) Parameters \(parameters)")
        
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
            
            switch response.result {
                
            case .success:
                if let data = response.data {
                    
                    
                    do {
                        
                        let res = try JSONDecoder().decode(Delivery.self, from: data)
                        
                        print(res.response)
                        
                        if res.invoices.count > 0 {
                            self.arr = res.invoices
                            self.tableView.reloadData()
                        }
                        
                        
                    } catch let error {
                        print(error)
                    }
                    
                    
                }
                
            case .failure(let error):
                print(error)
                
            }
            
            self.indicator.stopAnimating()
            self.refreshControl.endRefreshing()
            
        }
        
    }
    
    
    @IBAction func btnLogOut() {
        
        logout()
        
    }
    
    
    
    @IBAction func switchViewControl(_ sender: UISegmentedControl) {
        
        self.arr.removeAll()
        self.tableView.reloadData()
        
        switch sender.selectedSegmentIndex {
        case 0:
            indicator.startAnimating()
            getDeliveryList()
            isDeliveryView = true
            break
            
        case 1:
            indicator.startAnimating()
            getHoldItems()
            isDeliveryView = false
            break
            
        default:
            break
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        
        let data = arr[0]
        print(data.city ?? "")
        
        
        // Create a new variable to store the instance of PlayerTableViewController
        let vc = segue.destination as! DetailVC
        vc.data = data
    }
}


extension DeliveryVC: BarcodeScannerCodeDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print("Barcode Data: \(code)")
        print("Symbology Type: \(type)")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            controller.resetWithError()
        }
    }
}

extension DeliveryVC: BarcodeScannerErrorDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error)
    }
}

extension DeliveryVC: BarcodeScannerDismissalDelegate {
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}


extension DeliveryVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "idnDeliveryCell") as! DeliveryCell
        cell.addItem(arr[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 133
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "idnDetail", sender: self)
        
    }
    
}
