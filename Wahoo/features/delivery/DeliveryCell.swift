
import Foundation
import UIKit

class DeliveryCell: UITableViewCell {
    
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblInvoiceNumber: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    
    func addItem(_ data: _Invoice) {
        
        lblCityName.text = data.city?.uppercased()
        lblAddress.text = data.receiverAddress
        lblInvoiceNumber.text = data.barcode
        lblPhoneNumber.text = data.contactNo
        
    }
    
    
    
}
