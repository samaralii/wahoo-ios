
import Foundation
import UIKit
import EPSignature
import Alamofire


class DetailVC: BaseVC {
    
    
    @IBOutlet weak var lblInvoiceNo: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblSubCompanyName: UIButton!
    
    @IBOutlet weak var lblItems: UILabel!
    
    @IBOutlet weak var lblNote: UILabel!
    
    @IBOutlet weak var btnPhoneNumber: UIButton!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    public var data: _Invoice?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initz()
    }
    
    
    private func initz() {
        
        if let val = data?.barcode {
            lblInvoiceNo.text = val
        }
        
        if let val = data?.receiverAddress {
            lblAddress.text = val
        }
        
        if let val = data?.senderName {
            lblSubCompanyName.setTitle(val, for: .normal)
        }
        
        if let val = data?.item {
            lblItems.text = val
        }
        
        if let val = data?.note {
            lblNote.text = val
        }
        
        if let val = data?.contactNo {
            btnPhoneNumber.setTitle(val, for: .normal)
        }
        
        if let val = data?.price {
            lblPrice.text = "\(val)"
        }
        
        
    }
    
    
    @IBAction func btnPhone(_ sender: Any) {
        if let val = data?.contactNo {
            onCall(val)
        }
    }
    
    @IBAction func btnSubComPhone(_ sender: Any) {
        
        if let val = data?.senderContactNo {
            onCall(val)
        }
        
    }
    
    private func onCall(_ number: String) {
        print("phone")
        let url: NSURL = URL(string: "TEL://\(number)")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    
    
    
    @IBAction func btnDelivered(_ sender: Any) {
        
        let signatureVC = EPSignatureViewController(signatureDelegate: self, showsDate: false, showsSaveSignatureOption: false)
        signatureVC.subtitleText = "I agree to the terms and conditions"
        signatureVC.title = "Signature"
        let nav = UINavigationController(rootViewController: signatureVC)
        present(nav, animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func btnCancel(_ sender: Any) {
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Reason", message: "Write Something", preferredStyle: .alert)
        
        //        2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = ""
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Cancel This Order", style: .cancel, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            if let note = textField?.text, !note.isEmpty {
                print("Text field: \(note)")
                self.onChangeStatus(note, ID_CANCELED)
            }
            
            
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func btnHold(_ sender: Any) {
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Reason", message: "Write Something", preferredStyle: .alert)
        
        //        2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = ""
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Hold This Order", style: .cancel, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            if let note = textField?.text, !note.isEmpty {
                print("Text field: \(note)")
                self.onChangeStatus(note, ID_ON_HOLD)
            }
            
            
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
        
        
        
        
    }
    
    
    private func onChangeStatus(_ note: String, _ status: Int) {
        
        
        let msg = status == ID_CANCELED ? "Your order has been cancelled" : "Your order has been on hold"
        
        var invoiceId: String = ""
        var subCompId: String = ""
        var invoiceNumber: String = ""
        
        if let val = data?.id {
            invoiceId = String(val)
        }
        
        if let val = data?.sub_company_id {
            subCompId = String(val)
        }
        
        if let val = data?.invoiceNumber {
            invoiceNumber = val
        }
        
        
        let parameters = ["invoiceId": invoiceId,
                          "sub_company_id": subCompId,
                          "status": "\(status)",
            "note": note,
            "invoiceNumber": invoiceNumber]
        
        
        let url = "\(BASE_URL)invoices/updatestatus"
        
        print("Url \(url) Parameters \(parameters)")
        
        indicator.startAnimating()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON{ response in
            
            switch response.result {
                
            case .success:
                if let data = response.data {
                    
                    
                    
                    do {
                        
                        let res = try JSONDecoder().decode(ApiResponse.self, from: data)
                        print(res.response)
                        
                        if res.response == "success" {
                            
                            let alert = UIAlertController(title: "Status", message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { [weak alert] (_) in
                                
                                self.performSegueToReturnBack()
                                
                            }))
                            
                            self.present(alert, animated: true)
                            
                        }
                        
                        
                        
                    } catch let error {
                        print(error)
                    }
                    
                    
                }
                
            case .failure(let error):
                print(error)
                
            }
            
            self.indicator.stopAnimating()
            
        }
        
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        performSegueToReturnBack()
    }
    
    
    
    func performSegueToReturnBack()  {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    
    func requestWith(imageData: Data?){
        
        
        var invoiceId: String = ""
        var subCompId: String = ""
        var invoiceNumber: String = ""
        
        if let val = data?.id {
            invoiceId = String(val)
        }
        
        if let val = data?.sub_company_id {
            subCompId = String(val)
        }
        
        if let val = data?.invoiceNumber {
            invoiceNumber = val
        }
        
        
        let parameters = ["invoiceId": invoiceId,
                          "sub_company_id": subCompId,
                          "status": "\(ID_DELIVERED)",
            "invoiceNumber": invoiceNumber]
        
        
        let url = "\(BASE_URL)invoices/updatestatus"
        
        print("Url \(url) Parameters \(parameters)")
        
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        
        indicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            
            if let data = imageData{
                multipartFormData.append(data, withName: "receivingSignature", fileName: "sig.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if response.error != nil{
                        print(response.error ?? "Error")
                        return
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                
            }
            
            
            self.indicator.stopAnimating()
        }
    }
    
    
    
}



extension DetailVC: EPSignatureDelegate {
    
    func epSignature(_: EPSignatureViewController, didSign signatureImage: UIImage, boundingRect: CGRect) {
        print("Yes")
        
        requestWith(imageData: UIImagePNGRepresentation(signatureImage))
        
    }
    
    func epSignature(_: EPSignatureViewController, didCancel error: NSError) {
        print("No")
    }
    
}
