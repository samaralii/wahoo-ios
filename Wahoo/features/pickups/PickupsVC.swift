
import Foundation
import UIKit
import Alamofire
import BarcodeScanner

class PickupsVC: BaseVC {
    
    
    @IBOutlet weak var pickupTable: UITableView!
    @IBOutlet weak var viewNoItem: UIView!
    var count: Int = 1
    
    var arr = [_Invoice]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initz()
    }
    
    
    func initz() {
        pickupTable.delegate = self
        pickupTable.dataSource = self
        showNoItemView(true)
    }
    
    @IBAction func openBarcodeScanner(_ sender: Any, forEvent event: UIEvent) {
        let viewController = makeBarcodeScannerViewController()
        viewController.title = "Barcode Scanner"
        present(viewController, animated: true, completion: nil)
        
        
        //        getInvoice("0001")
        
        
    }
    
    private func showNoItemView(_ hide: Bool) {
        
        if hide {
            pickupTable.isHidden = true
            viewNoItem.isHidden = false
        } else {
            pickupTable.isHidden = false
            viewNoItem.isHidden = true
        }
        
    }
    
    @IBAction func submitPickups(_ sender: Any, forEvent event: UIEvent) {
        
        let json = convertArrToJson(arr)
        confirmPickups(json ?? "{}")
        
        
    }
    
    
    private func addNewItem(_ data: _Invoice) {
        arr.insert(data, at: 0)
        pickupTable.reloadData()
    }
    
    private func makeBarcodeScannerViewController() -> BarcodeScannerViewController {
        let viewController = BarcodeScannerViewController()
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        return viewController
    }
    
    
    @IBAction func btnLogOut() {
        logout()
    }
    
    
    private func getInvoice(_ code: String) {
        
        let parameters = ["barcode": code]
        
        
        let url = "\(BASE_URL)invoices/getinvoice"
        
        print("Url \(url) Parameters \(parameters)")
        
        indicator.startAnimating()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON{ response in
            
            switch response.result {
                
            case .success:
                if let data = response.data {
                    
                    
                    
                    do {
                        
                        let invoice = try JSONDecoder().decode(GetInvoice.self, from: data)
                        
                        print(invoice.response)
                        
                        if invoice.response == "success" {
                            if let val = invoice.invoice {
                                self.addNewItem(val)
                                self.showNoItemView(false)
                            }
                        }
                        
                        
                    } catch let error {
                        print(error)
                    }
                    
                    
                }
                
            case .failure(let error):
                print(error)
                
            }
            
            self.indicator.stopAnimating()
            
        }
        
    }
    
    
    private func confirmPickups(_ jsonData: String) {
        
        let parameters = ["InvoicesIds": jsonData]
        
        
        let url = "\(BASE_URL)invoices/saveinvoices"
        
        print("Url \(url) Parameters \(parameters)")
        
        indicator.startAnimating()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON{ response in
            
            switch response.result {
                
            case .success:
                if let data = response.data {
                    
                    
                    
                    do {
                        
                        let res = try JSONDecoder().decode(ApiResponse.self, from: data)
                        
                        print(res.response)
                        
                        if res.response == "success" {
                            
                            self.arr.removeAll()
                            self.pickupTable.reloadData()
                            
                        }
                        
                        
                        
                    } catch let error {
                        print(error)
                    }
                    
                    
                }
                
            case .failure(let error):
                print(error)
                
            }
            
            self.indicator.stopAnimating()
            
        }
        
    }
    
    
}


extension PickupsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "idnPickupsCell") as! PickupsCell
        cell.addItem(arr[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            self.arr.remove(at: indexPath.row)
            self.pickupTable.beginUpdates()
            self.pickupTable.deleteRows(at: [indexPath], with: .automatic)
            self.pickupTable.endUpdates()
        }
    }
    
}

extension PickupsVC: BarcodeScannerCodeDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print("Barcode Data: \(code)")
        print("Symbology Type: \(type)")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            controller.resetWithError()
        }
    }
}

extension PickupsVC: BarcodeScannerErrorDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error)
    }
}

extension PickupsVC: BarcodeScannerDismissalDelegate {
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}


