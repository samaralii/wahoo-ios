

import Foundation
import UIKit


class PickupsCell: UITableViewCell {
    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblInvoice: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    
    
    func addItem(_ data: _Invoice) {
       
        lblName.text = data.receiverName ?? ""
        lblInvoice.text = data.invoiceNumber ?? ""
        lblContact.text = data.contactNo ?? ""
        
    }
    
   
}
