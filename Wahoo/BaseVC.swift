import Foundation
import UIKit


class BaseVC: UIViewController {
    
    private let KEY_USERDATA = "userData"
    
    
    var indicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        indicator.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        indicator.center = view.center
        
        let transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        indicator.transform = transform
        
        self.view.addSubview(indicator)
        self.view.bringSubview(toFront: indicator)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    
    func showAlert(_ title: String ,_ msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    
    func setUserData(_ data: Data) {
        UserDefaults.standard.set(data, forKey: KEY_USERDATA)
        UserDefaults.standard.synchronize()
    }
    
    
    func setIsFirstTime(_ b: Bool) {
        UserDefaults.standard.set(b, forKey: IS_FIRST_TIME)
        UserDefaults.standard.synchronize()
    }
    
    func getIsFirstTime() -> Bool {
        return UserDefaults.standard.bool(forKey: IS_FIRST_TIME)
    }
    
    
    func getUserData() -> UserLogin? {
        do {
            let userData = UserDefaults.standard.object(forKey: KEY_USERDATA) as? Data
            if let data = userData {
                return try JSONDecoder().decode(UserLogin.self, from: data)
            } else {
                return nil
            }
        } catch let errorJson {
            print("Error While Parsing Json", errorJson)
            return nil
        }
    }
    
    func clearUserData() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
            print("Delete UserDefaults")
        }
    }
    
    func logout() {
        
        clearUserData()
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "idnLoginVC") as! LoginVC
        self.present(newViewController, animated: true, completion: nil)
    }
    
    
    
}
