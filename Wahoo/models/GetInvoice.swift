

import Foundation

struct GetInvoice: Decodable {
    let response: String
    let invoice: _Invoice?
}


struct IdsList: Codable {
    let id: [String]
}
