import Foundation


struct Invoice: Decodable {
    let response: String
    let invoices: _Invoice?
}

struct _Invoice: Decodable {
    var id: Int?
    var invoiceNumber: String?
    var receiverName: String?
    var contactNo: String?
//    var airwaybill_id: String?
    var sub_company_id: Int?
    var barcode: String?
    var receiverAddress: String?
//    var country: String?
    var item: String?
    var city: String?
    var price: Double?
//    var date: String?
//    var status: String?
//    var created: String?
//    var modified: String?
//    var pickupDateTime: String?
//    var diliveryCharges: Double?
    var note: String?
    var senderName: String?
    var senderContactNo: String?
    
}
