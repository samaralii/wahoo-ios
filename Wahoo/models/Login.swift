import Foundation

struct UserLogin: Decodable {
    let response: String
    let user: User
}

struct User: Decodable {
    let id: Int
    let email: String
    let phone: String
}
