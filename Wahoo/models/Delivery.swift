import Foundation


struct Delivery: Decodable {
    let response: String
    let invoices: [_Invoice]
}
